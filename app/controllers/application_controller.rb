class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: "¡hello, SON!"
  end
  def goodbye
    render html: "Goodbye, SON!"
  end
end
